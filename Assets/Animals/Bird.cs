﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : Animal
{

    private float lifetime = 120;

    private float timer = 0;


    void OnDisable()
    {
        timer = 0;
    }


    private void Start()
    {
        playercam = FindObjectOfType<PlayerCamera>(); 
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;


        //destruir gameobject despues de un tiempo largo para que no quede suelto por la escena.
        if (timer > lifetime)
            this.gameObject.SetActive(false);

        this.transform.Translate(new Vector3(0.5f * Time.deltaTime, 0, -3f * Time.deltaTime));

        if(Input.GetMouseButtonDown(0))
        {
            //first check if animal is actually in camera viewport
            if (!Visible)
                return;

            //if it is, then calculate min distance from touchpoint to animal to trigger camera
            float proximity = CalcProximity();
            float minProximity = 12f - Mathf.Min(10,Mathf.Sqrt(Distance)); //comparacion dinamica basado en que tan lejos (profundidad) esta el animal 

            //compare proximity to triggerProximity
            if (proximity < 0)
            {
                Debug.LogError("negative proximity is not allowed, something went wrong");
            }
            else if(proximity < minProximity)  
            {
                Debug.Log("clicked " + proximity.ToString() + "meters near " + this.gameObject.name);
                GetComponent<Renderer>().material.color = Color.green;
                //its close!  assign target;
                playercam.Target = this; 
            }
            else
            {
                Debug.Log(this.gameObject.name + " is" + proximity.ToString() + ".  Too far for photo!  Needs to be at least: " + minProximity);
                //not close enough to trigger camera. reset.
            }
        }
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public abstract class Animal : MonoBehaviour
{
    protected PlayerCamera playercam;

    public bool activated = false;

    public Animal[] children;

    public float Distance
    {
        get
        {
            return Vector3.Distance(this.transform.position, playercam.transform.position);
        }
    }

    public bool Visible
    {
        get
        {
            Vector3 screenPoint = playercam.cam.WorldToViewportPoint(this.transform.position);
            if (screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1)
            {
                return true;  //inside playercam viewport
            }
            else
            {
                return false;  //not visible to player
            }
        }
    }


    public void Activate()
    {
        //que se mueva, que haga su animacion o lo que sea.
        //todo

        //que tambien active sus hijos si tiene
        if(children.Length > 0)
        {
            foreach(Animal child in children)
            {
                if(child != null)
                    child.Activate();
            }
        }
    }

    public void Reset()
    {
        // aca hacer que el animal vuelva a su start position y se desactiva.
    }


    //proximity to photocam
    public float CalcProximity()
    {
        Plane plane = new Plane(Camera.main.transform.forward, transform.position);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float dist;
        float proximity;
        if (plane.Raycast(ray, out dist))
        {
            Vector3 v3Pos = ray.GetPoint(dist);
            proximity = Vector3.Distance(v3Pos, transform.position);
        }
        else
        {
            proximity = -1; // couldnt get intersection
        }

        return proximity;

    }



}

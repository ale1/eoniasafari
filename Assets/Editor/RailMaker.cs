﻿using System;
using UnityEditor;
using UnityEngine;


public class RailMaker : EditorWindow
{

#if UNITY_EDITOR

    static string spacing = "      ";


    private BezierHandle touchingHandle;
    private Bezier touchingBezier;
    private Path touchingPath; 

    public static RailMaker Instance { get; private set; }
    public static bool IsOpen
    {
        get { return Instance != null; }
    }

    // Add menu item named "My Window" to the Window menu
    [MenuItem("Safari Tools/RailMaker")]
    public static void ShowWindow()
    {
        //Type inspectorType = Type.GetType("UnityEditor.InspectorWindow,UnityEditor.dll");
        EditorWindow.GetWindow<RailMaker>();   
    }

    private void OnEnable()
    {
        Instance = this;
    }


    void OnGUI()
    {

        GUILayout.Space(20);

        string text1 = "";
        if (touchingPath)
            text1 = touchingPath.gameObject.name;

        GUILayout.Label("Current Path Selected:", EditorStyles.miniLabel);
        GUILayout.Label(spacing + text1, EditorStyles.boldLabel);
        EditorGUILayout.BeginHorizontal();
            EditorGUI.BeginDisabledGroup(touchingPath == null);
            if (GUILayout.Button("Add Rail Segment"))
            {
                touchingPath.AddSegment();
            }
            EditorGUI.EndDisabledGroup();
       
        EditorGUILayout.EndHorizontal();
        

        ///
        DrawUILine(Color.grey);
        ///


        string text2 = "";
        if (touchingBezier)
            text2 = touchingBezier.gameObject.name;

        GUILayout.Label("Current Segment Selected:", EditorStyles.miniLabel);
        GUILayout.Label(spacing + text2, EditorStyles.boldLabel);
        GUILayout.BeginHorizontal();

            EditorGUI.BeginDisabledGroup(!touchingBezier || !touchingBezier.siblingPrev);
            if (GUILayout.Button("<-Prev", EditorStyles.miniButtonLeft))
            {
                Selection.activeGameObject = touchingBezier.siblingPrev.gameObject;
                SceneView.FrameLastActiveSceneView();
                
            }
            EditorGUI.EndDisabledGroup();

            EditorGUI.BeginDisabledGroup(!touchingBezier || !touchingBezier.siblingAfter);
            if (GUILayout.Button("Next->", EditorStyles.miniButtonRight))
            {
                Selection.activeGameObject = touchingBezier.siblingAfter.gameObject;
                touchingHandle = touchingBezier.controlPoints[3].GetComponent<BezierHandle>();
                SceneView.FrameLastActiveSceneView();
            }
            EditorGUI.EndDisabledGroup();
         GUILayout.EndHorizontal();


         EditorGUI.BeginDisabledGroup(!touchingBezier);
         GUILayout.BeginHorizontal();
            if(GUILayout.Button("Add Signal",EditorStyles.miniButtonMid))
            {
                AddSignal();
            }
         GUILayout.EndHorizontal();
        EditorGUI.EndDisabledGroup();



        ///
        DrawUILine(Color.grey);
        ///


        string text3 = "";
        if (touchingHandle)
            text3 = touchingHandle.gameObject.name + "--Type: " + touchingHandle.handleType.ToString();
        GUILayout.Label("Current Handle Selected:", EditorStyles.miniLabel);
        GUILayout.Label(spacing + text3, EditorStyles.boldLabel);

        GUILayout.BeginHorizontal();
        EditorGUI.BeginDisabledGroup(touchingHandle == null  );
        if(GUILayout.Button("<--Prev"))
        {
            int index = (int)touchingHandle.handleType - 1;
            if (index < 1)
            {
                Selection.activeGameObject = touchingBezier.siblingPrev.gameObject;
                SceneView.FrameLastActiveSceneView();
                Selection.activeGameObject = touchingBezier.siblingPrev.controlPoints[3].gameObject;
            }
            else
            {
                Selection.activeGameObject = touchingBezier.controlPoints[index].gameObject;
            }
           
        }
        EditorGUI.EndDisabledGroup();

        EditorGUI.BeginDisabledGroup(touchingHandle == null);
        if(GUILayout.Button("Next-->"))
        {
            int index = (int)touchingHandle.handleType + 1;
            if (index > 3)
            {
                Selection.activeGameObject = touchingBezier.siblingAfter.gameObject;
                SceneView.FrameLastActiveSceneView();
                Selection.activeGameObject = touchingBezier.siblingAfter.controlPoints[1].gameObject;
            }
            else
            {
                Selection.activeGameObject = touchingBezier.controlPoints[index].gameObject;
            }
        }
        EditorGUI.EndDisabledGroup();

        GUILayout.EndHorizontal();




        ///
        DrawUILine(Color.grey);
        ///


        GUILayout.Space(50);

   


        EditorGUI.BeginDisabledGroup(touchingBezier == null);
        if(GUILayout.Button("Fix Terrain"))
        {
            Marker[] markers = touchingBezier.GetComponentsInChildren<Marker>();

            if (markers[markers.Length - 1].transform.position.y > markers[0].transform.position.y)
                Array.Reverse(markers);

            foreach(Marker mark in markers)
            {
                FixTerrain(mark.transform);
            }
                
        }


        EditorGUI.EndDisabledGroup();

    }


    void OnSelectionChange()
    {
        Debug.Log("hello");
        CheckPathTouches();
        CheckBezierTouches();
        CheckHandleTouches();
        Repaint();
    }


    private void CheckPathTouches()
    {
        Path[] selects = Selection.GetFiltered<Path>(SelectionMode.TopLevel);
        Path selected = null;
        if (selects.Length == 1)
            selected = selects[0];

        if (selected != null && touchingPath == null ) // first direct touch
        {
            touchingPath = selected;
            touchingBezier = touchingPath.beziers[0];
        }
        else if(selected != null && touchingPath != null) // switched Paths
        {
            touchingPath = selected;
            touchingBezier = touchingPath.beziers[0];
        }
    }


    private void CheckBezierTouches()
    {
        Bezier[] selects = Selection.GetFiltered<Bezier>(SelectionMode.TopLevel);
        if(selects.Length == 1)
        {
            touchingBezier = selects[0];
            touchingPath = touchingBezier.GetComponentInParent<Path>();
        }
        else if(selects.Length == 0 && touchingBezier != null)
        {
            touchingBezier = null;
        }
    }


    private void CheckHandleTouches()
    {
        BezierHandle[] selects = Selection.GetFiltered<BezierHandle>(SelectionMode.TopLevel);
        BezierHandle selected = null;
        if (selects.Length == 1)
            selected = selects[0]; 


        if (selected != null && touchingHandle == null) // first touch
        {
            touchingHandle = selects[0];
            touchingPath = touchingHandle.GetComponentInParent<Path>();
            touchingBezier = touchingHandle.GetComponentInParent<Bezier>();
            //Debug.Log("bezierhandle touch start");
        }
        else if(selected != null && touchingHandle != null && touchingHandle != selected)  //switched handles
        {
            touchingHandle = selected;
            touchingPath = touchingHandle.GetComponentInParent<Path>();
            touchingBezier = touchingHandle.GetComponentInParent<Bezier>();
            //Debug.Log("switched handles");
            touchingPath.RefreshPath();
        }
        else if (selected == null && touchingHandle != null) //released touch
        {
           //Debug.Log("bezierhandle released");
           touchingHandle.GetComponentInParent<Path>().RefreshPath();
         
           touchingHandle = null;

        }
    }

    //cuando el tab esta visible
    void OnBecameVisible()
    {
        FindObjectOfType<PathController>().EditMode = true;

        GameObject selectionGO = Selection.activeGameObject;
        if (selectionGO && selectionGO.GetComponent<Path>())
        {
            touchingPath = selectionGO.GetComponentInParent<Path>();
            touchingBezier = touchingPath.beziers[0];
        }
        else if(selectionGO && selectionGO.GetComponent<Bezier>())
        {
            touchingBezier = selectionGO.GetComponent<Bezier>();
            touchingPath = selectionGO.GetComponentInParent<Path>();
        }
        else if(selectionGO && selectionGO.GetComponent<BezierHandle>())
        {
            touchingHandle = selectionGO.GetComponent<BezierHandle>();
            touchingBezier = selectionGO.GetComponentInParent<Bezier>();
            touchingPath = selectionGO.GetComponentInParent<Path>();
        }
        else
        {
            Path path = FindObjectOfType<Path>();
            Selection.activeGameObject = path.gameObject;
            touchingPath = path;
            touchingPath.RefreshPath();
            touchingBezier = path.beziers[0];
            
        }


        SceneView.FrameLastActiveSceneView();



    }

    //cuando el tab se vuelve invisible (e.g te fuiste a otro tab).
    void OnBecameInvisible()
    {
        FindObjectOfType<PathController>().EditMode = false;

        touchingPath = null;
        touchingHandle = null;
        touchingBezier = null;
    }

    //cuando cerras el tab;
    private void OnDestroy()
    {

    }



#region signals


    public void AddSignal()
    {
        GameObject go = new GameObject();
        go.name = "Signal";
        SignalHandle sig = go.AddComponent<SignalHandle>();
        //UnityEditorInternal.ComponentUtility.MoveComponentUp(sig);
        //UnityEditorInternal.ComponentUtility.MoveComponentUp(sig);

        go.transform.parent = touchingBezier.GetComponentsInChildren<Marker>()[0].transform;
        go.transform.position = go.transform.parent.position + new Vector3(0, 6f, 0);

    }


#endregion


    public void FixTerrain(Transform spot)
    {

        int posXInTerrain; // position of the game object in terrain width (x axis)
        int posYInTerrain; // position of the game object in terrain height (z axis)

        int size = 3; // the diameter of terrain portion that will raise under the game object

        Terrain terr = Terrain.activeTerrain;
        int hmWidth = terr.terrainData.heightmapResolution;
        int hmHeight = terr.terrainData.heightmapResolution;


        // get the normalized position of this game object relative to the terrain
        Vector3 tempCoord = (spot.transform.position - terr.gameObject.transform.position);
        Vector3 coord;
        coord.x = tempCoord.x / terr.terrainData.size.x;
        coord.y = tempCoord.y / terr.terrainData.size.y;
        coord.z = tempCoord.z / terr.terrainData.size.z;

        // get the position of the terrain heightmap where this game object is
        posXInTerrain = (int)(coord.x * hmWidth);
        posYInTerrain = (int)(coord.z * hmHeight);

        // we set an offset so that all the raising terrain is under this game object
        int offset = size / 2;

        // get the heights of the terrain under this game object
        float[,] heights = terr.terrainData.GetHeights(posXInTerrain - offset, posYInTerrain - offset, size, size);

        // we set each sample of the terrain in the size to the desired height
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                heights[i, j] = coord.y;
            }
                
        }
      
        // set the new height
        terr.terrainData.SetHeights(posXInTerrain - offset, posYInTerrain - offset, heights);
    }




    //Editor Drawing Utils.
    public static void DrawUILine(Color color, int thickness = 2, int padding = 10)
    {
        Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(padding + thickness));
        r.height = thickness;
        r.y += padding / 2;
        r.x -= 2;
        r.width += 6;
        EditorGUI.DrawRect(r, color);
    }


#endif

}
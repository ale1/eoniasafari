﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;


[CustomEditor(typeof(SignalHandle))]
public class SignalHandleEditor : Editor
{

#if UNITY_EDITOR

    public override void OnInspectorGUI()
    {
        //DrawDefaultInspector();

        SignalHandle selectedSignal = (target as SignalHandle);


        if (selectedSignal.signals.Count != selectedSignal.GetComponents<Signal>().Length)
            refreshSignalList(selectedSignal);

        EditorGUILayout.BeginHorizontal();
        EditorGUI.BeginDisabledGroup(true);
        EditorGUILayout.LabelField("Marker");
        EditorGUILayout.ObjectField(selectedSignal.marker, typeof(Marker), true);
        EditorGUI.EndDisabledGroup();
        EditorGUILayout.EndHorizontal();


       if(GUILayout.Button("Add speed signal"))
       {
            selectedSignal.gameObject.AddComponent<SignalSpeed>();
        
       }

       if(GUILayout.Button("Add track switch"))
        {
            selectedSignal.gameObject.AddComponent<SignalSwitch>();
        }

       if(GUILayout.Button("Add camfocus signal"))
        {
            selectedSignal.gameObject.AddComponent<SignalCam>();
        }

    }


    private void refreshSignalList(SignalHandle selectedSignal)
    {
        selectedSignal.signals = new List<Signal>(selectedSignal.GetComponents<Signal>());
    }


#endif
}

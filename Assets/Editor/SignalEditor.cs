﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

[CustomEditor(typeof(Signal))]
public class SignalEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Signal selectedSignal = (target as Signal);


        GUILayout.Label("these signals will be active during levels:");
        EditorGUI.BeginChangeCheck();
        string values = EditorGUILayout.TextField("Object Name: ", selectedSignal.levels.ToString());

        if(EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(target, "touched levels through handle");
            selectedSignal.levels = ConvertoToIntArray(values);
        }



        if (selectedSignal is SignalSpeed)
        {
            SignalSpeed sig = (SignalSpeed)selectedSignal;

            EditorGUI.BeginChangeCheck();

            float speed = EditorGUILayout.Slider(sig.Speed, 0f, 10f);

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "touched values through custom editor");
                sig.Speed = speed;
            }
        }


        if (selectedSignal is SignalSwitch)
        {
            SignalSwitch sig = (SignalSwitch)selectedSignal;

            EditorGUI.BeginChangeCheck();

            Path targetPath = (Path)EditorGUILayout.ObjectField("target path", sig.targetPath, typeof(Path), true);

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "touched values through custom editor");
                sig.targetPath = targetPath;
            }
        }

        if(selectedSignal is SignalCam)
        {
            SignalCam sig = (SignalCam)selectedSignal;
            EditorGUI.BeginChangeCheck();

            Transform targetFocus = (Transform)EditorGUILayout.ObjectField("target focus", sig.focuspoint, typeof(Transform), true);

            if(EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "touch cam focus through editor");
                sig.focuspoint = targetFocus;
            }
        }


       
    }

    private int[] ConvertoToIntArray(string str)
    {
        return (int[]) (str ?? "").Split(',').Select<string, int>(int.Parse);
    }
}

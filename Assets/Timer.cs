﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    private float time = 0;
    private Text text;

    // Start is called before the first frame update
    void Start()
    {
        time = 0;
        text = GetComponent<Text>();

    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        text.text = Mathf.Round(time).ToString();
    }
}

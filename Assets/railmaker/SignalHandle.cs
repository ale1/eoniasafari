﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

public class SignalHandle : MonoBehaviour
{

#if UNITY_EDITOR

    [HideInInspector]
    public Marker marker;

    private bool selected;


    public List<Signal> signals = new List<Signal>(); 
    
    private bool EditMode = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnDrawGizmos()
    {
        if (marker == null)
            FindNearestMarker();

        if(EditMode == true || Vector3.Distance(UnityEditor.SceneView.lastActiveSceneView.camera.transform.position, this.transform.position) < 50f)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(this.transform.position, marker.transform.position);

            Gizmos.color = Color.red;
            Gizmos.DrawLine(this.transform.position, marker.waypoint.Pos);
        }



        Gizmos.DrawIcon(this.transform.position + new Vector3(0,1f,0), "IconSignal",true);

        if (UnityEditor.Selection.activeGameObject == this.transform.gameObject) 
        {
            selected = true;
        }

        //deselected
        if(selected == true && UnityEditor.Selection.activeGameObject != this.transform.gameObject)
        {
            selected = false;
            FindNearestMarker();
            GetComponentInParent<Path>().RefreshPath();
        }
    }

    public void UpdateWaypoint()
    {
        List<Signal> goSignals = GetComponents<Signal>().ToList();


        Waypoint wp = marker.waypoint;
        wp.signals = goSignals;

    }


    private void FindNearestMarker()
    {
        float minDist = 50f;

        Path path = GetComponentInParent<Path>();
        Marker[] markers = path.GetComponentsInChildren<Marker>();
        foreach (Marker mark in markers)
        {
            float dist = Vector3.Distance(this.transform.position, mark.transform.position);
            if (dist < minDist)
            {
                minDist = dist;
                marker = mark;
            }
        }

        if (marker == null)
            Debug.LogError("could not find nearby markers for this signal");

        if (this.transform.parent != marker.transform)
        {
            this.transform.SetParent(marker.transform, true);
            this.transform.position = new Vector3(transform.position.x, marker.transform.position.y + 5f, transform.position.z);
        }     
           
    }

    public void SetEditMode(bool value)
    {
        EditMode = value;
    }

   
#endif

}



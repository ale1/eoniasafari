﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignalSwitch : Signal
{

    public bool immediate = false;

    [SerializeField]
    public Path targetPath;
}

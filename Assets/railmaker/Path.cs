﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEditor;

public class Path : MonoBehaviour
{

    public bool EditMode { get; set; }

    public int id;

    public Transform startingPoint = null;

    public GameObject durmientePrefab;
    public GameObject segmentPrefab;

    public Transform SegmentsContainer;

    public List<Bezier> beziers;


    [SerializeField]
    private List<Segment> segments = new List<Segment>();


    public Segment GetSegment(int index)
    {
        if (segments.Count <= index)
            return null;
        else
            return segments[index];
    }


#if UNITY_EDITOR


    public void RefreshPath()
    {
       
        beziers = SegmentsContainer.GetComponentsInChildren<Bezier>().ToList();

        segments = new List<Segment>();

        for( int i = 0; i < beziers.Count; i++)
        {
            
            beziers[i].AttachToOthers(startingPoint);
            
            segments.Add(beziers[i].BuildSegment());
        }

    }


    public void AddSegment()
    {
        Bezier lastCP = null;

        if (SegmentsContainer.GetComponentInChildren<Bezier>())
            lastCP = SegmentsContainer.GetComponentsInChildren<Bezier>().Last();

        GameObject newSegment = Instantiate(segmentPrefab, SegmentsContainer);
        newSegment.transform.SetAsLastSibling();
        if(lastCP != null)
            newSegment.transform.position = lastCP.controlPoints[3].position;
        newSegment.gameObject.name = "Segment" + (SegmentsContainer.GetComponentsInChildren<Bezier>().Length - 1);

        if(lastCP != null)
        {
            newSegment.GetComponent<Bezier>().controlPoints[0].transform.position = lastCP.controlPoints[3].transform.position;
            newSegment.GetComponent<Bezier>().controlPoints[1].transform.position = lastCP.controlPoints[3].transform.position + lastCP.transform.forward * 20;
            newSegment.GetComponent<Bezier>().controlPoints[2].transform.position = lastCP.controlPoints[3].transform.position + lastCP.transform.forward * 40;
            newSegment.GetComponent<Bezier>().controlPoints[3].transform.position = lastCP.controlPoints[3].transform.position + lastCP.transform.forward * 60;
        }

        RefreshPath();
    }


        public void OnEditMode(bool value)
    {

        foreach (var bez in beziers)
            bez.SetEditMode(value);

        SignalHandle[] Signals = GetComponentsInChildren<SignalHandle>(true); 
        foreach(SignalHandle sig in Signals)
        {
            sig.SetEditMode(value);
        }

    }

    [ContextMenu("Open Rail Maker")]
    public void OpenRailMaker()
    {
        EditorApplication.ExecuteMenuItem("Safari Tools/RailMaker");
    }

#endif





}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class BezierHandle : MonoBehaviour
{

    #if UNITY_EDITOR

    public enum HandleType
    {
        Start,
        Mid1,
        Mid2,
        End
    }

    public HandleType handleType;

    private void Awake()
    {
        transform.hasChanged = false;
    }

    private Bezier bez;

    // Start is called before the first frame update
    void Start()
    {
        bez = GetComponentInParent<Bezier>();
    }

    // Update is called once per frame
    void Update()
    {
        if (handleType == HandleType.Start || handleType == HandleType.End)
            return;


        if (Selection.activeGameObject != this.gameObject)
        {

            if (transform.hasChanged)
            {
               // Debug.Log(this.gameObject.name + " has finished moving!");
                transform.hasChanged = false;
            }

        }

        else if(Selection.activeGameObject == this.gameObject)
        {       
            if(handleType == HandleType.Mid2 && bez.siblingAfter != null)
            {     
                float distance = Vector3.Distance(this.transform.position, bez.controlPoints[3].position) + Vector3.Distance(bez.controlPoints[3].position, bez.siblingAfter.controlPoints[1].position);
                Vector3 direction = (bez.controlPoints[3].position - this.transform.position).normalized;
                Vector3 targetPos = this.transform.position + direction  * distance;
                bez.siblingAfter.controlPoints[1].transform.position = targetPos;
            }
            else if (handleType == HandleType.Mid1 && bez.siblingPrev != null)
            {
                float distance = Vector3.Distance(this.transform.position, bez.controlPoints[0].position) + Vector3.Distance(bez.controlPoints[0].position, bez.siblingPrev.controlPoints[2].position);
                Vector3 direction = (bez.controlPoints[0].position - this.transform.position).normalized;
                Vector3 targetPos = this.transform.position + direction * distance;
                bez.siblingPrev.controlPoints[2].transform.position = targetPos;
            }
        }
    }

    void OnDrawGizmosSelected()
    {
        bez = GetComponentInParent<Bezier>();

        Gizmos.DrawLine(bez.controlPoints[2].position, bez.controlPoints[3].position);

        Gizmos.DrawLine(bez.controlPoints[1].position, bez.controlPoints[0].position);

        if(bez.siblingAfter != null)
        {
            Gizmos.DrawLine(bez.controlPoints[3].position, bez.siblingAfter.controlPoints[1].position);
        }
            
        if(bez.siblingPrev != null)
        {
            Gizmos.DrawLine(bez.siblingPrev.controlPoints[2].position, bez.controlPoints[0].position);
        }

    }


    public bool EditMode { get; private set; }
    public void SetEditMode(bool value)
    {
        EditMode = value;

        if(EditMode == false)
        {
            GetComponent<Collider>().enabled = false;
        }
        else
        {
            GetComponent<Collider>().enabled = true;
        }

    }

#endif

}

   

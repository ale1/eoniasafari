﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Bezier : MonoBehaviour
{

 #if UNITY_EDITOR

    [SerializeField]
    public Transform[] controlPoints;

    private Vector3 gizmosPosition;

    private float interval = 0.025f;

    public Transform lastCP
    {
        get
        {
            return controlPoints[3];
        }
    } 
    public Bezier siblingAfter { get; private set; }
    public Bezier siblingPrev { get; private set; }



#region gizmos
    void OnDrawGizmos()
    {

        if (EditMode == false)
            return;


        if(interval <= 0 )
        {
            Debug.LogError("invalid internal value");
            return;
        }

        for(float t = 0; t <= 1; t += interval)
        {
            gizmosPosition = Mathf.Pow(1 - t, 3) * controlPoints[0].position +
                3 * Mathf.Pow(1 - t, 2) * t * controlPoints[1].position +
                3 * (1 - t) * Mathf.Pow(t, 2) * controlPoints[2].position +
                Mathf.Pow(t, 3) * controlPoints[3].position;
      
            Gizmos.DrawSphere(gizmosPosition, 0.15f);
        }
 
    }

    void OnDrawGizmosSelected()
    {
        if (EditMode == false)
            return;

        Gizmos.DrawLine(new Vector3(controlPoints[0].position.x, controlPoints[0].position.y, controlPoints[0].position.z),
        new Vector3(controlPoints[1].position.x, controlPoints[1].position.y, controlPoints[1].position.z));

        Gizmos.DrawLine(new Vector3(controlPoints[2].position.x, controlPoints[2].position.y, controlPoints[2].position.z),
        new Vector3(controlPoints[3].position.x, controlPoints[3].position.y, controlPoints[3].position.z));
    }
#endregion


    // para curvas mas custom, donde los controlpoints definen la curva.
    public Segment BuildSegment()
    {

        //todo: refactor: usar path.cs para contar children bezier en vez de cada bezier contando por su cuenta.

        int hierarchy = transform.GetSiblingIndex(); // para saber si tiene hermanos
        if (hierarchy > 0)
            siblingPrev = transform.parent.GetChild(hierarchy - 1).gameObject.GetComponent<Bezier>();
        else
            siblingPrev = null;

        if (hierarchy < transform.parent.childCount - 1)  // last segment
            siblingAfter = transform.parent.GetChild(hierarchy + 1).gameObject.GetComponent<Bezier>();
        else
            siblingAfter = null;


        // contorlpoints[1],[2] => estos se usan como estan el gameObject
        controlPoints[3].position = new Vector3(controlPoints[3].position.x, controlPoints[3].position.y, controlPoints[3].position.z);


        //rotation
        controlPoints[1].transform.LookAt(controlPoints[0]);
        controlPoints[2].transform.LookAt(controlPoints[3]);
        controlPoints[3].transform.LookAt(2 * controlPoints[3].transform.position - controlPoints[2].transform.position);


        if (siblingPrev != null) //entoces tiene hermanos mayores
        {
            //ajustamnos automaticamente posicion de CP1 para alinearse con CP2 del hermano mayor

            float distance = Vector3.Distance(siblingPrev.controlPoints[2].position, siblingPrev.controlPoints[3].position) + Vector3.Distance(controlPoints[0].position, controlPoints[1].position);
            controlPoints[1].transform.position = siblingPrev.controlPoints[2].position + siblingPrev.controlPoints[2].transform.forward * distance; 

        }

        //Dynamic Interval
        CalculateOptimalInterval();

        List<Waypoint> waypoints = new List<Waypoint>();

        int index = 0;
        for (float t = 0; t <= 1; t += interval)  
        {
           
            Vector3 pos = Mathf.Pow(1 - t, 3) * controlPoints[0].position +
                3 * Mathf.Pow(1 - t, 2) * t * controlPoints[1].position +
                3 * (1 - t) * Mathf.Pow(t, 2) * controlPoints[2].position +
                Mathf.Pow(t, 3) * controlPoints[3].position;

            Waypoint waypoint = new Waypoint(index, pos);
            waypoints.Add(waypoint);

            GameObject prefab = GetComponentInParent<Path>().durmientePrefab;

            Transform markerTrf = this.transform.Find("marker" + index.ToString());
            Marker marker;

            if(markerTrf == null)
            { 
                GameObject go = Instantiate(prefab);
                marker = go.GetComponent<Marker>();
                go.transform.parent = this.transform;
                go.transform.position = pos;
                go.gameObject.name = "marker" + index.ToString();
                marker = go.GetComponent<Marker>();
                marker.waypoint = waypoint;
            }
            else
            {
                marker = markerTrf.GetComponent<Marker>();
                marker.transform.position = pos;
                marker.waypoint = waypoint;
            }

            if (index != 0)  // nos salteamos el index 0 porque lo quremos hacer el final
            { 
                //look away in opposite direction
                marker.transform.LookAt(2 * pos - waypoints[index-1].Pos);
                marker.Distance = Vector3.Distance(waypoint.Pos, waypoints[index - 1].Pos);
                
            }

            waypoint.rot = marker.transform.eulerAngles;

            index++;
        }

        //borramos markers viejos si hay demasiados (porque se achico la distancia entre re-builds)
        int counter = index;  //last index used
        while( this.GetComponentsInChildren<Marker>().Length >= index + 1)
        {
            Marker lastMarker = this.GetComponentsInChildren<Marker>().Last();
            //Debug.Log(lastMarker.gameObject.name + " was destroyed");
            DestroyImmediate(lastMarker.gameObject);
        }


#region specialCaseFirstMarker
        //hacemos el index = 0 al final porque necesitamos index 1 ya calculado para darle angulo entre marker 1 y segmento anterior


        if ( hierarchy == 0)  //es el primer track, no tiene hermano mayor
        {
            Marker marker0 = GetComponentsInChildren<Marker>().First();
            Marker marker1 = GetComponentsInChildren<Marker>()[1];
            marker0.transform.LookAt(marker1.transform);
        }
        else
        {
            Transform lastMarker = siblingPrev.GetComponentsInChildren<Marker>().Last().transform;
            Marker marker0 = GetComponentsInChildren<Marker>().First();
            Marker marker1 = GetComponentsInChildren<Marker>()[1];
            marker0.transform.rotation = Quaternion.Lerp(lastMarker.rotation, marker1.transform.rotation, 0.5f);
            marker0.Distance = Vector3.Distance(lastMarker.position, marker0.transform.position);
        }
#endregion


        RefreshSignals();

        Segment segment = new Segment(waypoints);
        return segment;
    }


    private void RefreshSignals()
    {
        SignalHandle[] signals =  GetComponentsInChildren<SignalHandle>();
        foreach(SignalHandle sig in signals)
        {
            sig.UpdateWaypoint();
              
        }
    }

    


    public void CalculateOptimalInterval()
    {
        List<Vector3> points = new List<Vector3>();
        for (float t = 0; t <= 1; t += 0.05f)  //hardcoded internval to get some reference points for distance calculation
        {

            Vector3 point = Mathf.Pow(1 - t, 3) * controlPoints[0].position +
                3 * Mathf.Pow(1 - t, 2) * t * controlPoints[1].position +
                3 * (1 - t) * Mathf.Pow(t, 2) * controlPoints[2].position +
                Mathf.Pow(t, 3) * controlPoints[3].position;

            points.Add(point);
        }

        float distance = 0;

        for(int i = 1; i < points.Count; i++)
        {
            distance += Vector3.Distance(points[i], points[i - 1]);
        }

        float desiredMarkers = distance / 1.5f;
        interval = 1 / desiredMarkers;

     }





    public void AttachToOthers(Transform startingPoint)
    {
        // no tiene hermano mayor, es el primer track
        if(siblingPrev == null)
        {
    
            if (startingPoint == null)  //si no tiene, entonces se deja como esta (start point manual seteado en al escena)
            {
                controlPoints[0].gameObject.SetActive(true); //el start estaba esocndido, ahora hay que mostrarlo.
            }
            else
            {

                controlPoints[0].transform.position = startingPoint.position;
            }
            
            return;
        }

        Transform siblingEndpoint = siblingPrev.controlPoints[3].transform;
        Transform siblingCP = siblingPrev.controlPoints[2].transform;

        //positioning
        //Vector3 offset = siblingEndpoint.position - controlPoints[0].position;
        controlPoints[0].position = siblingEndpoint.position;


        //como tiene hermano mayor, no mostramos el start contorlpoint[0] GO,
        //ya que siempre debe ser igual al end CP del hermano y no queremos humano tocandolo.
        controlPoints[0].gameObject.SetActive(false);


    }

    public bool EditMode { get; private set; }
    public void SetEditMode(bool value)
    {
        EditMode = value;

        foreach(Transform trf in controlPoints)
        {
            trf.GetComponent<Renderer>().enabled = EditMode;
            trf.GetComponent<BezierHandle>().SetEditMode(EditMode);
        }

       
    }

#endif
}

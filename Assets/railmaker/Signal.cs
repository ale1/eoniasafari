﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Signal : MonoBehaviour
{

    public bool Active = true;
    public int[] levels;
}

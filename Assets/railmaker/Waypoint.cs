﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Waypoint
{
    public Vector3 Pos;
    public Vector3 rot;
    public int Id;
   
    public Transform FocusPoint = null;
    public PlayerMover.playerPos PlayerPos = PlayerMover.playerPos.NONE;

    public List<Signal> signals = new List<Signal>();


    //contstructors
    public Waypoint(int id, Vector3 pos)
    {
        Id = id;
        Pos = pos;
    }

    public Waypoint(int id, Vector3 pos, Transform focusPoint = null, PlayerMover.playerPos playerPos = PlayerMover.playerPos.NONE, float trainSpeed = -1)
    {
        Id = id;
        Pos = pos;
        FocusPoint = focusPoint;
        PlayerPos = playerPos;
    }

}

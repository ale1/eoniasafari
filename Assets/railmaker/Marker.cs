﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Marker : MonoBehaviour
{
    #if UNITY_EDITOR

    public bool durmienteVisible = true;
    public float distance = 1f;
    public float Distance
    {
        get { return distance; }
        set
        {
            float scaling = 2.8f;
            railLeft.transform.localScale = new Vector3(railLeft.transform.localScale.x, railLeft.transform.localScale.y, value * scaling );
            railRight.transform.localScale = new Vector3(railRight.transform.localScale.x, railRight.transform.localScale.y, value * scaling);
            distance = value;
        }
    }

    public GameObject railLeft;
    public GameObject railRight;

    public Waypoint waypoint;

 

#endif


}

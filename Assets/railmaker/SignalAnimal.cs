﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignalAnimal : Signal
{

    public List<Animal> animals;

    public void ActivateAnimals()
    {
        foreach(Animal animal in animals)
        {
            animal.Activate();
        }
    }
  
}

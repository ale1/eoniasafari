﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignalCam : Signal
{

    [HideInInspector]
    public Transform focuspoint;


    [HideInInspector]
    public PlayerMover.playerPos PlayerPos = PlayerMover.playerPos.NONE;


}

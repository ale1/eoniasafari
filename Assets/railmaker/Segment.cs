﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class Segment
{
    [SerializeField]
    public List<Waypoint> waypoints;


   //cons
   public Segment(List<Waypoint> _waypoints)
   {
        string str = "";
        waypoints = new List<Waypoint>(_waypoints);
        foreach(var wp in waypoints)
        {
            str += wp.Pos.ToString();
        }
    }

   public Waypoint GetWaypoint(int index)
   {
        if (waypoints.Count <= index)
            return null;
        else
            return waypoints[index];
   }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class PathController : MonoBehaviour
{

#if UNITY_EDITOR
    private bool editMode;
    public bool EditMode
    {
        get { return EditMode; }
        set
        {
            editMode = value;

            Path[] paths = FindObjectsOfType<Path>();
            foreach (var path in paths)
                path.OnEditMode(editMode);

        }
    }


#endif

    // Start is called before the first frame update
    void Start()
    {
           
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public int gamespeed { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        Reset();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void SpeedUp()
    {
        gamespeed = Mathf.Min(gamespeed++, 3);
        Time.timeScale = gamespeed;
    }

    public void SpeedDown()
    {
        gamespeed = Mathf.Max(gamespeed--, 0);
        Time.timeScale = gamespeed;
    }

    public void Reset()
    {
        gamespeed = 1;
        Time.timeScale = gamespeed;
    }

    public void TogglePause()
    {
        if (gamespeed == 0)
            gamespeed = 1;
        else
            gamespeed = 0;

        Time.timeScale = gamespeed;
    }
}

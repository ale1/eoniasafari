﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class Wagon : MonoBehaviour
{
    private Train _train;

    private float Timer;
    private float startDistance;
    private float currentDistance;
    private Quaternion startRot;

    public Path currentPath;
    public Path nextPath;  
    private int segmentIndex;
    private Segment currentSegment;
    private int waypointIndex;
    private Waypoint currentWaypoint;

    public Action<Waypoint> OnWaypointAcquired;


    public void Reset(Train train, Path path)
    {

        currentPath = path;
        nextPath = null;
        _train = train;

        segmentIndex = 0;
        waypointIndex = 0;
        currentSegment = currentPath.GetSegment(segmentIndex);
        currentWaypoint = currentSegment.GetWaypoint(waypointIndex);

        startDistance = Vector3.Distance(this.transform.position, currentWaypoint.Pos);
        startRot = this.transform.rotation;
        Timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(currentPath != null)
            FollowPath();
   
    }

    private void FollowPath()
    {
        //currentDistance = Vector3.Distance(this.transform.position, currentWaypoint.pos);
        Timer += Time.deltaTime * _train.trainSpeed;

        //train movement
        transform.position = Vector3.MoveTowards(transform.position, currentWaypoint.Pos, Time.deltaTime * _train.trainSpeed);


        transform.rotation =  Quaternion.Lerp(startRot, Quaternion.Euler(currentWaypoint.rot), Timer / 3f);


        if(Vector3.Distance(transform.position,currentWaypoint.Pos) <= 0.1f)
        {
            //get next waypoint
            currentWaypoint = currentSegment.GetWaypoint(++waypointIndex);

            if(currentWaypoint == null)
            {
                waypointIndex = 0;
                currentSegment = currentPath.GetSegment(++segmentIndex);

                if (currentSegment == null)
                {
                    FinishedPath();
                    return;
                }
                    
                currentWaypoint = currentSegment.GetWaypoint(waypointIndex);

            }

            startDistance = Vector3.Distance(this.transform.position, currentWaypoint.Pos);
            startRot = this.transform.rotation;
            Timer = 0f;

            OnWaypointAcquired?.Invoke(currentWaypoint);
        }

    }

    private void FinishedPath()
    {
        if (nextPath != null)
        {
            currentPath = nextPath;
            nextPath = null;
        }
        else
        {
            if (_train.Loop)
            {
                Reset(_train, _train.startPath);
            }
            else
            {
                Debug.LogError("finished path!");
                //do something e.g: StopAtStation()
            }

        }

       
        
    }
}

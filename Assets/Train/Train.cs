﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Train : MonoBehaviour
{
    public int level;

    [Tooltip("cual vagon va a leer los track signals y avisarle a los otros.")]
    public Wagon LeadWagon;

    public float trainSpeed;
    public Transform camFocus;

    public bool Loop;

    public Path startPath;

    private Wagon[] wagons;

    // Start is called before the first frame update
    void Start()
    {
        wagons = GetComponentsInChildren<Wagon>();
        foreach( Wagon wagon in wagons )
        {
            wagon.Reset(this,startPath);
        }

        LeadWagon.OnWaypointAcquired += CheckSignal;
    }


    private void CheckSignal(Waypoint waypoint)
    {

        //waypoint data
        if (waypoint.FocusPoint != null)
        {
            FindObjectOfType<PlayerCamera>().Focuspoint = waypoint.FocusPoint;
            Debug.Log("Focused changed by signal");

        }

        if (waypoint.PlayerPos != PlayerMover.playerPos.NONE)
        {
            FindObjectOfType<PlayerMover>().targetPlayerPos = waypoint.PlayerPos;
            Debug.Log("Player Pos changed by signal");
        }


        //Signal data
        List<Signal> signals = waypoint.signals.Where(x=> x.Active).ToList();
        foreach (Signal signal in signals)
        {
            if (!signal.levels.Contains(this.level))
                continue;
            
            switch (signal)
            {
                case SignalSpeed signalSpeed:
                    ChangeSpeed(signalSpeed.Speed);
                    break;
                case SignalSwitch signalSwitch:
                    ChangeTrack(signalSwitch.targetPath, signalSwitch.immediate);
                    break;
                case SignalCam signalCam:
                    FindObjectOfType<PlayerCamera>().Focuspoint = signalCam.focuspoint;  //todo
                    FindObjectOfType<PlayerMover>().targetPlayerPos = signalCam.PlayerPos;  //todo: no usar FindObjectofType. 
                    break;
                case SignalAnimal signalAnimal:
                    signalAnimal.ActivateAnimals();
                    break;
                default:
                    Debug.LogError("signal not recognized");
                    break;


            }
        }
    }


    private void ChangeSpeed(float targetSpeed)
    {
            trainSpeed = targetSpeed;
            Debug.Log("changed speed due to signal");
    }

    private void ChangeTrack(Path targetPath, bool inmediate)
    {
        foreach(var wagon in wagons)
        {
            if(inmediate)
            {
                wagon.Reset(this, targetPath);
            }
            wagon.nextPath = targetPath;
        }
        
    }


     
    
}

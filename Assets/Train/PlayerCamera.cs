﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCamera : MonoBehaviour
{
    public PlayerMover playerMover;

    public Transform Focuspoint;

    public Camera cam { get; private set; }

    private bool locked = false;
    private Vector3 lockedPosition;

    private Animal target = null;
    public Animal Target
    {
        get { return target; }
        set {
            target = value;
            if (value != null)
            {
                photoeffect.Play("testanim");
            }
            else
            {
                photoeffect.Play("testanimReverse");
            }
     
        }
    }
    private Quaternion defaultRot;
    public Transform defaultPos;
    private float rotSpeed = 5f;
    private float zoomSpeedStart = 20f;
    private float zoomSpeed;

    public Animator photoeffect;

    private int layerMask;


    private void Awake()
    {
        layerMask = 1 << 9;
    }

    // Start is called before the first frame update
    void Start()
    {
        defaultRot = this.transform.rotation;
        cam = GetComponent<Camera>();
        zoomSpeed = zoomSpeedStart;
       
    }

    // Update is called once per frame
    void Update()
    {

        //check target is still valid
        if(Target != null && !Target.Visible)
        {
            Target = null;
        }

        //lose target if finger no longer touching screen 
        if (Target != null && Input.GetMouseButtonUp(0) && !locked)
        {
            StartCoroutine(TakePhoto());
            Target.GetComponent<Renderer>().material.color = Color.red;
        }


        //if target has passed the tests above
        if(Target != null)
        {

            //rotation towards target
            Vector3 direction = Target.transform.position - transform.position;
            Quaternion toRotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Lerp(transform.rotation, toRotation, rotSpeed * Time.deltaTime);

            //if zoomed to max we take photo,we want to continue angle focus, but freeze zoom
            if (Vector3.Distance(this.transform.position, Target.transform.position) < 15)
            {
                if (locked) //already taking photo
                    this.transform.position = lockedPosition;
                else
                    StartCoroutine(TakePhoto());

                return;
            }
                

            //if focused angle on camera, now can start zooming
            Vector3 CameraCenter = cam.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, cam.nearClipPlane));
            RaycastHit hit;  
            if (Physics.Raycast(CameraCenter, transform.forward, out hit, 200, layerMask))
            {
                if(hit.transform == Target.transform)
                {
                    //zoom towards target with aceleration
                    zoomSpeed += Time.deltaTime * 150;
                    transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, zoomSpeed * Time.deltaTime);
                }       
            }

  
               
        }
        else
        {
            FocusCamera();

            zoomSpeed -= Time.deltaTime * 150;
            transform.position = Vector3.MoveTowards(transform.position, defaultPos.position, Mathf.Max(zoomSpeedStart, zoomSpeed) * Time.deltaTime);


            if (Vector3.Distance(this.transform.position, defaultPos.position) < 0.1f)  //volviste al deault pos
            {
                zoomSpeed = zoomSpeedStart;

            }

        }


    }


    public void FocusCamera()
    {
        if (Focuspoint == null)
            return;

        Vector3 lookDirection = (Focuspoint.position - transform.position).normalized;
        defaultRot = Quaternion.LookRotation(lookDirection);

        transform.rotation = Quaternion.Lerp(transform.rotation, defaultRot, rotSpeed * 0.1f * Time.deltaTime);

        Debug.DrawLine(transform.position, Focuspoint.position , Color.cyan);
    }

    
    //public void TurnLeft()
    //{
    //    Vector3 rot = defaultRot.eulerAngles;
    //    rot = new Vector3(rot.x, rot.y - rotationDegrees, rot.z);
    //    defaultRot = Quaternion.Euler(rot);
    //}

    //public void TurnRight()
    //{
    //    Vector3 rot = defaultRot.eulerAngles;
    //    rot = new Vector3(rot.x, rot.y + rotationDegrees, rot.z);
    //    defaultRot = Quaternion.Euler(rot);
    //}


    private IEnumerator TakePhoto()
    {
        locked = true;
        lockedPosition = this.transform.position;
        //todo take photo here

        yield return new WaitForSeconds(0.8f);
        FindObjectOfType<Flash>().doCameraFlash = true;
        yield return new WaitForSeconds(0.2f);
        Debug.Log("took photo!");
        locked = false;
        Target = null;
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayerMover : MonoBehaviour
{

    public enum playerPos
    {
        NONE,
        FRONT,
        RIGHT,
        LEFT,
        BACK
    }

    public Transform playerPosFront;
    public Transform playerPosRight;
    public Transform playerPosLeft;
    public Transform playerPosBack;

    public playerPos targetPlayerPos = playerPos.FRONT;


    // Start is called before the first frame update
    void Awake()
    {

    }

    private void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (targetPlayerPos == playerPos.NONE)
            return;

        Transform trf = playerPosFront;
        switch((int)targetPlayerPos)
        {
            case 0:
                Debug.LogError("should get here");
                break;
            case 1:
                trf = playerPosFront;
                break;
            case 2:
                trf = playerPosRight;
                break;
            case 3:
                trf = playerPosBack;
                break;
            default:
                Debug.LogError("hmmm");
                break;
        }

        this.transform.position = Vector3.MoveTowards(this.transform.position, trf.position ,  Time.deltaTime);
    }

}
